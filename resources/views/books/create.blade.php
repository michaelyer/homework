<h1>Add new Book</h1>

<form action="{{action('BookController@store')}}" method = 'post'>
    {{csrf_field()}}

    <div class = "form-group">
        <label for="title"> The name of the book you would like to add?</label>
        <input type="text" class="form-control" name = 'title'>
    </div>
    <div>
        <label for="title"> Name of the author of the book: </label>
        <input type="text" class = "form-control" name = 'author'>
    </div>

    <div class = "form-group">
        <input type="submit" class = "form-control" name = "submit" value = "Save">
    </div>
</form>