<h1>Edit Your Book</h1>

<form action="{{action('BookController@update',$books->id)}}" method = 'post'>
    @csrf
    @method('PATCH')
    <div class = "form-group">
        <label for="title"> The name of the book you would like to edit?</label>
        <input type="text" class="form-control" name = 'title' value = "{{$books->title}}">
    </div>
    <div>
        <label for="title"> Name of the author of the book: </label>
        <input type="text" class = "form-control" name = 'author' value= "{{$books->author}}">
    </div>

    <div class = "form-group">
        <input type="submit" class = "form-control" name = "submit" value = "Save">
    </div>
</form>

<form method ='post' action="{{action('BookController@destroy',$books->id)}}">
@csrf
@method('DELETE')
<div class ="form-group">
    <input type="submit" class = "form-control" name ="sumbit" value="delete book">
</div>
</form>
