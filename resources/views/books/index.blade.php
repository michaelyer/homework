<h1>This is your Books Table</h1>
<table>
    <tr>
        <th>
            User Id
        </th>
        <th>
            Book Title
        </th>
        <th>
            Author Name
        </th>
    </tr>
    
    @foreach($books as $book)
    <tr>
        <td>
            {{$book->user_id}}
            </td>
            <td>
            <a href="{{route('books.edit',$book->id)}}">{{$book->title}}</a>
            </td>
            <td>
            {{$book->author}}
            </td>
    @endforeach
    </tr>
</table>
<br>
<a href="{{route('books.create')}}">Create new Book</a>