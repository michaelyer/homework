<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Noam',
                'email' => "noam@gmail.com",
                'password' => 12345,
                'created_at' => date('Y-m-d G:i:s')
            ],
            [
                'name' => 'Ami',
                'email' => "ami@gmail.com",
                'password' => 12345,
                'created_at' => date('Y-m-d G:i:s')
                ]
        ]);
    }
}
